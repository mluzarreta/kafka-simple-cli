from confluent_kafka import Producer, Consumer

from pathlib import Path
import argparse
import sys

from kafka_simple_cli.utils import colored_print
from kafka_simple_cli.config import PRODUCER_CONFIG, CONSUMER_CONFIG, CONSUMPTION_TIMEOUT


def produce(topic, msg, config_map):

    def delivery_callback(err, msg):
        if err:
            colored_print(f"Message failed delivery: {err}", "error")
        else:
            colored_print(f"Message delivered to {msg.topic()} @ {msg.offset()}", "success")
            print((msg.value()).decode('utf-8'))

    # Producer instance
    p = Producer(config_map)

    # Produce a message 
    for line in msg:
        
        # Remove empty lines
        if not line:
            continue

        try:
            # Produce line
            p.produce(topic, line, callback=delivery_callback)

        except BufferError:
            colored_print(f"Local producer queue is full ({len(p)} messages awaiting delivery): try again", "error")
        
        p.poll(0)

    colored_print(f"Waiting for {len(p)} deliveries", "info")

    p.flush()

def consume(topic, config_map, timeout):
    
    c = Consumer(config_map)

    def print_assignment(consumer, partitions):
        colored_print(f"Assignment: {partitions}", "info")

    c.subscribe([topic], on_assign=print_assignment)

    # Read messages from Kafka, print to stdout
    try:

        counter = 0

        # Consume messages until reach out the timeout
        while counter < (timeout + 1) :

            # Every second, try to pull msg from kafka topic
            msg = c.poll(timeout=1.0)
            
            # If no message is available, try again and increment the timeout counter
            if msg is None:
                counter += 1
                continue
            
            # Reinitialize the timeout if a message is retrieved
            counter = 0

            # Display the message error is any
            if msg.error():
                colored_print(msg.error(), "error")
            
            # Display a success log
            else:
                colored_print(f"Reading message at offset {msg.offset()}", "success")
                # Proper decoded message
                print(msg.value().decode('utf-8'))

        colored_print(f"Timeout reached ({CONSUMPTION_TIMEOUT}s)", "info")

    except KeyboardInterrupt:
        colored_print(f"Aborted by user", "warn")

    finally:
        # Close down consumer to commit final offsets.
        c.close()

def cli():

    # Parser declaration
    parser = argparse.ArgumentParser(
    description="Simple Kafka producer and consumer written in Python"
    )
    parser.add_argument("topic", type=str, help="The topic name where you want to send your message")
    parser.add_argument("message", type=str, help="The message to send")
    parser.add_argument("--bootstrap-servers", type=str, help="A comma-separated list of [HOST]:[PORT] kafka servers")
    args = parser.parse_args()

    # If the message argument is -, get the message from stdin
    if args.message == '-':
        args.message = sys.stdin.read().strip()

    # Split the message (one message by line)
    args.message = args.message.split('\n')

    # Check if message contain content. Elsewhere refuse to go further
    if not args.message:
        colored_print("Refuse to send an empty message !", "error")
        sys.exit(1)

    # Check if bootstrap.servers is configured, fail elsewhere
    if not PRODUCER_CONFIG.get('bootstrap.servers') and not args.bootstrap_servers:
        colored_print("Please provide a bootstrap server configuration via the cli or the configuration file", "error")
        sys.exit(1)

    # Update existing configuration with --bootstrap-servers value
    if args.bootstrap_servers:
        PRODUCER_CONFIG['bootstrap.servers'] = args.bootstrap_servers
        CONSUMER_CONFIG['bootstrap.servers'] = args.bootstrap_servers

    # Produce messages
    produce(args.topic, args.message, PRODUCER_CONFIG)
    
    # Consume them just after
    consume(args.topic, CONSUMER_CONFIG, CONSUMPTION_TIMEOUT)