import yaml
from pathlib import Path
import sys
import copy
import os

from kafka_simple_cli.utils import recursive_update, colored_print

# Retrieve all the different configuration files (package one, global one and user one)
pkg_config_path = Path(__file__).parent.resolve() / 'config/ksc.yml'
global_config_path = Path("/etc/ksc.yml")
xdg_config_home = os.environ.get("XDG_CONFIG_HOME")

# Try to get $XDG_CONFIG_HOME, use $HOME/.config base path elsewhere
if not xdg_config_home:
    usr_config_path = Path(os.environ.get("HOME")) / '.config/ksc.yml'
else:
    usr_config_path = Path(xdg_config_home) / 'ksc.yml'

# Load the package configuration
with open(pkg_config_path, 'r') as config_file:
    config_map = yaml.load(config_file, Loader=yaml.SafeLoader)

# Load the global configuration
if global_config_path.exists():
    # Update recursively the configuration
    with open(global_config_path, 'r') as global_config_file:
        try:
            config_map = recursive_update(config_map, yaml.load(global_config_file, Loader=yaml.SafeLoader))
        except yaml.parser.ParserError:
            colored_print(f"{global_config_path} malformed !","error")
            sys.exit(1)

# Load the user configuration
if usr_config_path.exists():
    # Update recursively the configuration
    with open(usr_config_path, 'r') as usr_config_file:
        try:
            config_map = recursive_update(config_map, yaml.load(usr_config_file, Loader=yaml.SafeLoader))
        except yaml.parser.ParserError:
            colored_print(f"{usr_config_path} malformed !","error")
            sys.exit(1)

client_config = config_map['common.connection']

# Transforms bootstrap.servers list in a comma-separated list if needed
if isinstance(client_config.get('bootstrap.servers'), list):
    client_config['bootstrap.servers'] = ','.join(client_config['bootstrap.servers'])

# PRODUCER_CONFIG, CONSUMER_CONFIG and CONSUMPTION_TIMEOUT are available for main.py
PRODUCER_CONFIG = client_config

CONSUMER_CONFIG = {}
CONSUMER_CONFIG.update(client_config)
CONSUMER_CONFIG.update(config_map['consumer.connection'])

CONSUMPTION_TIMEOUT = config_map['consumption.timeout']