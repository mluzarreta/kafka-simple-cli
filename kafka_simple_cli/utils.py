from datetime import datetime
import sys

log_level_color_map = {
    "info": "\033[1;34m",
    "success": "\033[0;32m",
    "warn": "\033[33m",
    "error": "\033[1;31m",
    }

def colored_print(msg, log_level):

    if log_level not in log_level_color_map.keys():
        raise AttributeError(f"{log_level} is not supported")

    if not sys.stdout.isatty():
        print(f"{datetime.now()} - {log_level.upper()} - {msg}")
    else :
        print(f"{log_level_color_map[log_level]}{datetime.now()} - {log_level.upper()} - {msg}\033[0m")


def recursive_update(orig, update):

    if not isinstance(orig, dict) or not isinstance(update, dict):
        raise TypeError('Params must be dicts')

    for key in update:
        if isinstance(update[key], dict) and isinstance(
                orig.get(key), dict):
            orig[key] = recursive_update(orig[key], update[key])
        else:
            orig[key] = update[key]

    return orig