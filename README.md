# Kafka simple CLI

## Description

This project hosts a very simple Kafka client written in python. It is delivered as a CLI (ksc).
The goal of this tool is to produce one or multiples messages in a topic and then consume it.

## Getting started

### Installation

```sh
git clone git@gitlab.com:mluzarreta/kafka-simple-cli.git
pip install kafka-simple-cli

# or directly

pip install git+https://gitlab.com/mluzarreta/kafka-simple-cli.git
```

### Usage

```sh
# Usage : ksc <topic> <message> [--bootstrap-servers '<broker_address>:<broker_port>']
ksc test 'This is my first message' --bootstrap-servers '<broker_address>:<broker_port>'

# Support piped messages
echo -e 'riri\nfifi\nloulou' | ksc test -
```

### Configuration

skc read configurations located into :
- '/etc/ksc.yml'
- '${XDG_CONFIG_HOME}/kafka-simple-cli/config.yml'

This configuration file is mainly used to set up the client connection. See the following examples :

#### Plaintext configuration 
```yaml
common.connection:
  bootstrap.servers:
    - 192.168.0.1:8080
    - 192.168.0.2:8080
    - 192.168.0.3:8080
```
#### SSL configuration 
```yaml
common.connection:
  bootstrap.servers:
    - 192.168.0.1:8080
    - 192.168.0.2:8080
    - 192.168.0.3:8080
  
  security.protocol: "SSL"
  # If self-signed certificate
  ssl.ca.location: "/path/to/bundle.pem"
```

#### SSL with client certificate 
```yaml
common.connection:
  bootstrap.servers:
    - 192.168.0.1:8080
    - 192.168.0.2:8080
    - 192.168.0.3:8080
  
  security.protocol: "SSL"
  # If self-signed certificate
  ssl.ca.location: "/path/to/bundle.pem"
  ssl.certificate.location: "/path/to/client.pem"
  ssl.key.location: "/path/to/client.key"
```
It is also possible to modify the consumer timeout (5 seconds by default):

```yaml
# Duration between the last retrieved message and the CLI exit, in seconds
consumption.timeout: 10
``` 